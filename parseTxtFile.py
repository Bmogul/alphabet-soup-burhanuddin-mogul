import trie

# parse_file extracts data out of text file
# dimensions of the grid
# the grid itself
# the words to find in the grid stored in a trie
def parse_file(fileName):
    with open(fileName, 'r') as file:
        # initializing variables
        dimensions = file.readline().strip().split('x');
        grid = []
        bank = []

        # looping through word grid and storing values in grid
        for i in range(int(dimensions[0])):
            grid.append(file.readline().strip().split())
    
        # stores all the words into the bank
        for line in file:
            bank.append(line.strip())
    
        return dimensions, grid, bank