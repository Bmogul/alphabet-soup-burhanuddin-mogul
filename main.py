import sys

import search
import parseTxtFile
import trie

# program accepts filepath for textfile as an argument
# if no argument is provided program asks user to enter
# filepath for desired word search file
if (len(sys.argv) <= 1):
    filePath = input('Please input path of file to parse: ')
else:
    filePath = sys.argv[1]

# Uses parseTextFile to retrive needed data
dimensions, grid, bank = parseTxtFile.parse_file(filePath)
bank_trie = trie.Trie()
for word in bank:
    bank_trie.insert(word)

# perform search for all words
result = search.WordSearch(bank_trie, grid, dimensions)

# print out result
for word in bank:
    print(result[word])