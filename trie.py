# Trie data structure to stor words of word search
# to optimize search code

class Node:
    def __init__(self):
        self.children = {}
        self.is_word = False

class Trie:
    def __init__(self):
        self.root = Node()
    
    def insert(self, word):
        node = self.root
        for char in word:
            if char not in node.children:
                node.children[char] = Node()
            node = node.children[char]
        node.is_word = True

    def print_trie(self, node=None, word=''):
        if not node:
            node = self.root

        if node.is_word:
            print(word)

        for c, child in node.children.items():
            self.print_trie(child, word + c)