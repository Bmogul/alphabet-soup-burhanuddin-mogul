import pytest

from trie import Trie
from search import WordSearch

# Sample test cases
test_cases = [
    {
        "grid": [
            ['a', 'b', 'c', 'd'],
            ['e', 'f', 'g', 'h'],
            ['i', 'j', 'k', 'l'],
            ['m', 'n', 'o', 'p']
        ],
        "dimensions": (4, 4),
        "bank": ['abc', 'afk', 'onm', 'kgc', 'mjg'],
        "expected": {
            'abc': 'abc 0:0 0:2',
            'afk': 'afk 0:0 2:2',
            'onm': 'onm 3:2 3:0',
            'kgc': 'kgc 2:2 0:2',
            'mjg': 'mjg 3:0 1:2'
        }
    },
    {
        "grid": [
            ['a', 'a', 'a', 'a'],
            ['a', 'b', 'a', 'a'],
            ['a', 'a', 'e', 'a'],
            ['a', 'x', 'a', 'a']
        ],
        "dimensions": (4, 4),
        "bank": ['abe', 'abax'],
        "expected": {
            'abe': 'abe 0:0 2:2',
            'abax': 'abax 0:1 3:1'
        }
    },
    {
        "grid": [
            ['a', 'b', 'c', 'd'],
            ['e', 'f', 'g', 'h'],
            ['i', 'j', 'k', 'l'],
            ['m', 'n', 'o', 'p']
        ],
        "dimensions": (4, 4),
        "bank": ['abcd', 'efgh', 'ijkl', 'mnop'],
        "expected": {
            'abcd': 'abcd 0:0 0:3',
            'efgh': 'efgh 1:0 1:3',
            'ijkl': 'ijkl 2:0 2:3',
            'mnop': 'mnop 3:0 3:3'}
    }
]

# Testing WordSearch function


@pytest.mark.parametrize("test_case", test_cases)
def test_WordSearch(test_case):
    bank = Trie()
    for word in test_case["bank"]:
        bank.insert(word)

    result = WordSearch(bank, test_case["grid"], test_case["dimensions"])
    assert result == test_case["expected"]
