import trie

# DFS algorithm to search grid for words in word bank
# DFS algo checks in one direction at a time
# specified by the dx and dy variables
def dfs(grid, row, col, word, dx ,dy):
    # base case: word has been found
    if (len(word) == 0):
        # subtract dx and dy so that correct
        # "coordinates" are returned
        return row-dx, col-dy
    # Check to see if within bounds and character
    # is apart of the word
    if ((row < len(grid) and row >= 0 and col >= 0 and col < len(grid[0]))  and grid[row][col] == word[0]):
        found = dfs(grid, row+dx, col+dy, word[1:], dx, dy)
        if found:
            return found
    
    # No solution is found
    return None

# DFS algo modified for use with a trie data stucture
def dfs_trie(grid, row, col, node, word, dx, dy, initial_row, initial_col, result):
    # base case
    if (node.is_word):
        result[word] = f"{word} {initial_row}:{initial_col} {row-dx}:{col-dy}"

    # check to see if within bounds
    if (row < len(grid) and row >= 0 and col >= 0 and col < len(grid[0])):
        # update visited
        # recursive call if coordinates have not been visited and correct letter
        if (grid[row][col] in node.children):
            child = node.children[grid[row][col]]
            dfs_trie(grid, row+dx, col+dy, child, word+grid[row][col], dx, dy, initial_row, initial_col, result)

    return

# Word search function, loops trhoguh word bank
# and grid to find all words and returns a dictionaty
# which has the cooridnates for each word 
def WordSearch(bank, grid, dimensions):
    rows, cols = dimensions
    result = {}
    directions = [(1, 1), (-1, 0), (-1, 1), (0, -1),
                       (0, 1), (1, -1), (1, 0), (-1, -1)]
    
    # loop for traversing grid and trie
    for row in range(int(rows)):
        for col in range(int(cols)):
            for dx, dy in directions:
                if grid[row][col] in bank.root.children:
                    tmpNode = bank.root.children[grid[row][col]]
                    dfs_trie(grid, row+dx, col+dy, tmpNode, grid[row][col], dx, dy, row, col, result)

    return result
